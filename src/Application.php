<?php declare(strict_types=1);

/**
 * @link https://bitbucket.org/linpax/microphp
 * @copyright Copyright &copy; 2017 Linpax
 * @license https://bitbucket.org/linpax/microphp/src/master/LICENSE
 */

namespace Micro;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;


interface Application
{
    public function __construct(Kernel $kernel);
    public function getConfig() : array;
    public function getContainer() : ContainerInterface;
    public function getKernel() : Kernel;
    public function run(RequestInterface $request) : ResponseInterface;
}
